const jwt = require("jsonwebtoken")

const secretKey = 'c#@!123'

const generateToken = (username, password) => {
    return jwt.sign({
		username: username,
		password: password
	}, secretKey, { expiresIn: '1h' })
}   

module.exports = generateToken