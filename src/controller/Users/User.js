const UserModel = require('../../model/user');
const LoginModel = require('../../model/login');

const { passwordCrypt } = require("../../_ultilities/passwordCrypt")

const UserController = new UserModel()
const LoginController = new LoginModel()

var User = {
    register: async function(data) {
        let login = LoginController.create(data.username, passwordCrypt(data.password), data.email);
        let user = UserController.create(data.username, passwordCrypt(data.password), data.email, data.fullname, data.address);
        return await (user, login)
    },

    getInfoUserByUsername: async function(username) {
        return await UserController.findByUsername(username)
    },
    getInfoUserByUserId: async function(id) {
        return await UserController.findByUserId(id)
    },
    getAllUsers: async function() {
        return await UserController.findAllUsers()
    },
    updateByUserId: async function(id, data) {
        let beforeUpdate = await UserController.findByUserId(id)

        if (beforeUpdate.length >= 1) {
            let user = await UserController.updateUserById(id, data)
            LoginController.updateByUsername(beforeUpdate[0].username, user.data)

            return user
        } else {
            return {
                success: 0,
                message: "Cannot fine user"
            }
        }
    },
    deleteByUserId: async function(id) {
        return await UserController.deleteByUserId(id)
    },
    deleteByUserUsername: async function(username) {
        return await UserController.deleteByUsername(username)
    }


}

module.exports = User