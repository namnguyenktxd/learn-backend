/**
 * Login Model
 */
const moment = require('moment');
const Abs = require('./abs')
const { comparePassword } = require('../_ultilities/passwordCrypt')
const tableName = 'login';

class LoginModel extends Abs {
    constructor() {
        super();
    }

    async create(username, password, email) {
        return await this.postgres.insert({
            username: username,
            password: password,
            email: email,
            created_at: moment().utc().format(),
            // logged_time: moment().utc().format()
        }).into(tableName).returning("*").then((resp) => {
            return {
                success: 1,
                data: resp[0],
                table: tableName
            }
        }).catch((err) => {
            console.log(err)
            return {
                status: 201,
                message: 'Create Error',
                detail: err.detail,
            };
        });
    }

    async login(data) {
        let current = moment().utc().format()
        return this.postgres.select('username', "password")
            .from(tableName)
            .where({ username: data.username })
            .returning("*")
            .then(resp => {
                if (resp.length >= 1) {
                    let isValidPassword = comparePassword(data.password, resp[0].password)
                    if (isValidPassword === true) {
                        this.postgres.select('*').from(tableName)
                            .update({
                                logged_time: current,
                                isLogin: true
                            }).returning('*').where({
                                username: data.username
                            }).then()
                        return {
                            success: 1,
                            message: "Log in success"
                        }
                    } else {
                        return {
                            success: 1,
                            message: "Wrong username or password"
                        }
                    }
                } else {
                    return {
                        success: 0,
                        message: `Account ${data.username} has not exist`
                    }
                }
            }).catch(err => {
                return err
            })
    }

    async updateByUsername(username, data) {
        return this.postgres.select("*")
            .from(tableName)
            .where({ username: username })
            .update({
                username: data.username,
                password: data.password,
                email: data.email
            }).returning("*").then(resp => {
                return {
                    success: 1,
                    data: resp
                }
            }).catch(err => {
                return {
                    success: 0,
                    data: err
                }
            })
    }

    async logout(username) {
        return this.postgres.select("*")
            .from(tableName)
            .where({ username: username })
            .update({
                isLogin: false
            }).returning("*").then(data => {
                if (data.length >= 1) {
                    return {
                        success: 1,
                        data: {
                            username: data[0].username,
                            email: data[0].email
                        }
                    }
                }
                return {
                    success: 0,
                    data: {}
                }
            })
    }

    async findByUsername(username) {
        return this.postgres.select("").from(tableName).where("username", "=", username).returning("*")
    }

    async forgotPassword(email) {
        return this.postgres
            .select("*")
            .from(tableName)
            .where('email', "=", email)
    }

    static initial(knex) {
        return knex.schema.createTable(tableName, (table) => {
            table.increments('id').unique().notNullable();
            table.text('username').unique().notNullable();
            table.text('password').notNullable();
            table.text('email').unique().notNullable();
            table.datetime('created_at').notNullable();
            table.datetime('logged_time');
            table.boolean('isLogin').defaultTo(false).notNullable()
        }).then().catch();
    }

    static drop(knex) {
        return knex.schema.dropTableIfExists(tableName).then().catch();
    }
}

module.exports = LoginModel;