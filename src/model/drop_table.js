/**
 * Create Table
 */
const postgres = require('../helper/postgres');
const User = require('./user');
const Login = require('./login');

(() => {
    User.drop(postgres)
    Login.drop(postgres)
})();