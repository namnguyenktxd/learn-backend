/**
 * Development config
 */
module.exports = {
    knex: {
        client: 'pg',
        connection: {
            host: '127.0.0.1',
            port: 5432,
            user: 'postgres',
            password: '123456',
            database: 'users'
        },
        pool: {
            min: 2,
            max: 10
        }
        // migrations: {
        //     directory: __dirname + "/database/users "
        // }
    }
};