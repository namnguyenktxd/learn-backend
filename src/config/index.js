/**
 * Config
 */
const devConfig = require('./development');
const prodConfig = require('./production');

const ENV = process.env.NODE_ENV || 'development';

const config = {
  development: devConfig,
  production: prodConfig,
};

// default ENV is development
module.exports = config[ENV];
